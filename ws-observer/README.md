### `WS-Observer`
Provides a websocket interface to retrieve real time data such as the position and status of fish.