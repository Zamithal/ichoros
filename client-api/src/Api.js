import express from 'express';

export class Api {

	expressApp = undefined;
	
	routes = {
		gets: [
			{
				route: '/api/v1/routes',
				action: (req, res) => getRoutes(req, res, this.routes)
			},
			{
				route: '/api/v1/helloWorld',
				action: helloWorld
			}
		]
	}

	constructor() {		
		this.expressApp = express();

		this.routes.gets.forEach( ({route, action}) => {
			this.expressApp.get(route, action);
		});
	}

	// start listening on port with all routes
	start = (port) => {
		const onConnect = () => {
			console.log(`API listening on ${port}`)
		}

		this.expressApp.listen(port, onConnect);
	}
}

const getRoutes = (req, res, routes) => {
	console.log(`getRoutes: ${req.ip}`)

	const gets = routes.gets.map(route => route.route);

	res.status(200)
		.send({
			gets: gets
		});
}

const helloWorld = (req, res) => {
	console.log(`helloWorld: ${req.ip}`)
	res.status(200)
		.send({
			message: 'hello world'
		});
}