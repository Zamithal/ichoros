import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles, withWidth } from '@material-ui/core';
import { Typography, Button, IconButton } from '@material-ui/core';
import { Menu } from '@material-ui/icons';
import * as pages from '_services/static/pages';

class Header extends PureComponent {
	render() {
			
		const {classes: c} = this.props;
		const {width} = this.props;

		const headerBody = (width === 'sm' || width === 'xs')
			? compactHeader : fullHeader;

		return(
			<div className={c.root}>
				<svg viewBox='0 0 400 150' preserveAspectRatio='none'>
					<defs>
						<linearGradient id='grad' gradientTransform='rotate(90)'>
							<stop id='topGradient' offset='5%'  stopColor='white' />
							<stop id='bottomGradient' offset='95%' stopColor='black' stopOpacity='0' />
						</linearGradient>
					</defs>
					<path shapeRendering='optimizeQuality' fill='black'
						d='M 0 0 L 400 0 L 400 100 C 100 0 300 200 0 100 Z'/>
					<path shapeRendering='optimizeQuality' fill='url(#grad)'
						d='M 0 0 L 400 0 L 400 100 C 100 0 300 200 0 100 Z'/>
				</svg>
				{headerBody(this.props)}
			</div>
		);
	}
}

const fullHeader = (props) => {
	const {classes: c} = props;

	return(
		<div className={c.fullBody}>
			<Typography variant='h1' color='textPrimary'>IchorOS</Typography>
			{ Object.keys(pages).filter( 
				(pageName) => pages[pageName].displayHeader === true
			).map( (key) => 
				<Button key={pages[key].id} className={c.linkButton}>
					<Typography color='textPrimary'>
						{pages[key].name}
					</Typography>
				</Button>
			)}

		</div>
	);
}

const compactHeader = (props) => {
	const {classes: c} = props;

	return(
		<div className={c.compactBody}>
			<Typography variant='h1' color='textPrimary'>IchorOS</Typography>
			<IconButton>
				<Menu/>
			</IconButton>
		</div>
	);
}



Header.propTypes = {
	classes: PropTypes.object,
	currentPage: PropTypes.string.isRequired,
}

const propBind = (state) => {
	return({
		currentPage: state.navigation.pages.current,
	})
}

const styles = (theme) => ({
	root: {
		width: '100%',
		height: 150,
		'& > *': {
			position: 'absolute',
		},
		'& #topGradient': {
			stopColor: theme.palette.headerLight
		},
		'& #bottomGradient': {
			stopColor: theme.palette.background.paper
		},
		'& > svg': {
			width: '100%',
			height: '100%',
		},
		color: theme.palette.text.primary,
		filter: 'drop-shadow(0px 1px 3px rgba(0,0,0,0.9))',
	},
	fullBody: {
		display: 'flex',
		flexDirection: 'row',
		justifyContent: 'center',

		padding: theme.spacing.small,
	},
	compactBody: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		padding: theme.spacing.small,
		width: '100%',
	},
	linkButton: {
		'& > *': {
			paddingTop: 3,
		},
		marginLeft: theme.spacing.large,
	}
});

const styled = withStyles(styles)(Header);
const widthed = withWidth()(styled);
const connected = connect(propBind)(widthed);

export {connected as Header};