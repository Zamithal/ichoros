import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles, MuiThemeProvider, CssBaseline } from '@material-ui/core';
import { HashRouter, Switch, Route } from 'react-router-dom';
import * as themes from '_services/theme/';
import { connect } from 'react-redux';
import { DocTitle } from './DocTitle/DocTitle';
import { Header } from './Header/Header';
import * as pageComponents from './pages';
import * as pages from '_services/static/pages';

class App extends PureComponent {

	render() {
		const { theme } = this.props;
		return (
			<Fragment>
				<CssBaseline/>
				<DocTitle/>
				<MuiThemeProvider theme={themes[theme]}>
					<Header/>
					<HashRouter>
						<Switch>
							{ Object.keys(pages).map( (pageName) =>
								<Fragment key={pageName}>
									{ pages[pageName].routes.map( (route) =>
										<Route key={route}
											exact path={route} 
											component={pageComponents[pages[pageName].componentName]}/>
									)}	
								</Fragment>
							)}
							<Route component={pages['notFound']}/>
						</Switch>
					</HashRouter>
				</MuiThemeProvider>
			</Fragment>
		);
	}
}

App.propTypes = {
	theme: PropTypes.string.isRequired,
}

const propBind = (state) => {
	return ({
		theme: state.configuration.theme.current,
	})
}

const styles = () => ({

});

const styled = withStyles(styles)(App);
const connected = connect(propBind)(styled);
export { connected as App};
