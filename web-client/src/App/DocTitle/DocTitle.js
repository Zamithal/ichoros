import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { PureComponent } from 'react';

class DocTitle extends PureComponent {

	render() {

		const { pageName } = this.props;
		document.title = `IchorOS - ${pageName}`;

		return(null);
	}
}

DocTitle.propTypes = {
	pageName: PropTypes.string.isRequired,
}

const propBind = (state) => {

	return({
		pageName: state.navigation.pages.current,
	})
}

const connected = connect(propBind)(DocTitle);
export { connected as DocTitle };
