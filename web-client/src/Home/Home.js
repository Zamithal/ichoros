import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';
import { withStyles, Grid } from '@material-ui/core';
import { Typography, Paper, IconButton } from '@material-ui/core';
import { Bubbles } from 'Backgrounds';
import { home } from '_services/static/pages';
import * as icons from '_services/icons';

class Home extends PureComponent {

	render() {
		const {classes: c} = this.props;
		const { content } = home;
		return(
			<div className={c.root}>
				<Bubbles/>
				<div className={c.content}>
					<span style={{height: '20vh'}}/>
					<Paper square className={c.tanks}>
						<Typography variant={'h2'}>{content.tankTitle}</Typography>
						<Grid container spacing={8}>
							{ content.tanks.map( (tank) => {
								const TankIcon = icons[tank.icon];
								return(
									<Grid className={c.tank} key={tank.title} item xs={12} md={6} lg={3}>
										<IconButton>
											<TankIcon color='primary'/>
										</IconButton>
										<Typography variant={'h3'}>{tank.title}</Typography>
										<Typography>{tank.description}</Typography>
									</Grid>
								)
							})}
						</Grid>
					</Paper>
				</div>
			</div>
		);
	}
}


Home.propTypes = {
	classes: PropTypes.object,
}

const styles = (theme) => ({
	root: {
		marginTop: -100,
		position: 'relative',
	},
	content: {
		paddingTop: 100,
		width: '100%',
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	tanks: {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		width: '100%',
		padding: `5vh ${theme.spacing.large}px 5vh ${theme.spacing.large}px`,
	},
	tank: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'center',
		'& *': {
			textAlign: 'center'
		},
		'& > p': {
			width: '90%'
		},
		'& svg': {
			width: '8rem',
			height: '8rem',
		}

	}
});

const styled = withStyles(styles)(Home);

export {styled as Home};