import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';

class Bubbles extends PureComponent {

	render()
	{
		const { classes: c} = this.props;

		return (
			<svg className={c.root} width='100%' height='100%'>
				<defs>
					<pattern id='bubbles' x='0' y='0' width='300' 
						height='300' patternUnits='userSpaceOnUse'>
						<g fill='#dee3f6'>
							<circle strokeWidth='2' stroke='#66b3ff' cx='30' cy='80' r='10'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='50' cy='50' r='17'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='140' cy='120' r='20'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='200' cy='25' r='12'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='260' cy='60' r='14'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='40' cy='175' r='12'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='90' cy='260' r='17'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='250' cy='230' r='8'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='140' cy='20' r='15'/>
							<circle strokeWidth='2' stroke='#66b3ff' cx='260' cy='150' r='13'/>
						</g>
					</pattern>
				</defs>
				<rect x='0' y='0' width='100%' height='100%' fill='#f8f9fd'/>
				<rect x='0' y='0' width='100%' height='100%' fill='url(#bubbles)'/>
			</svg>
		);
	}
}


Bubbles.propTypes = {
	classes: PropTypes.object,
}

const styles = (theme) => ({
	root: {
		width: '100vw',
		height: '100vh',
		top: 0,
		left: 0,
		position: 'fixed',
		zIndex: -1,
	},
});

const styled = withStyles(styles)(Bubbles);
export { styled as Bubbles }