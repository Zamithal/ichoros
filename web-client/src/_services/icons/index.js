export { 
	Info as Home,
	Info as About,
	Info as StandardTank,
	Info as ApocalypseTank,
	Info as AlphaTank,
	Info as SymbioticTank
} from '@material-ui/icons';
