import { navigationReducer } from './navigation/reducer';
import { configurationReducer } from './configuration/reducer';
import { combineReducers } from 'redux';

export const reducer = combineReducers({
	navigation: navigationReducer,
	configuration: configurationReducer,
});