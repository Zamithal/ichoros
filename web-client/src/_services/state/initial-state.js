import { navigationState } from './navigation/initial-state';
import { configurationState } from './configuration/initial-state';

export const initialState = {
	navigation: navigationState,
	configuration: configurationState,
};