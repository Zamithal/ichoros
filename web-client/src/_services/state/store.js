import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { initialState } from './initial-state';
import { reducer } from './reducer';

export const store = createStore(
	reducer,
	initialState,
	applyMiddleware(thunk)
);

