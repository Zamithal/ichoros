export const home = {
	name: 'Home',
	id: 'home',
	description: 'Home Page',
	componentName: 'Home',
	iconName: 'Home',
	routes: ['/', '/Home'],
	displayHeader: true,
	displayFooter: true,
	displayNavTray: true,

	content: {
		tanksTitle: 'Tanks',
		tanks: [
			{
				title: 'Standard',
				description: 'A simple tank with no gimmicks. Fish complete without outside interference.',
				icon: 'StandardTank',
			},
			{
				title: 'Apocalypse',
				description: 'Death waves occur on a set interval that kill all fish outside a shelter',
				icon: 'ApocalypseTank',
			},
			{
				title: 'Alpha Predator',
				description: 'A handful of fish have superior speed, strength, and health',
				icon: 'AlphaTank',
			},
			{
				title: 'Symbiotic',
				description: 'Fish are paired up. If either fish dies, both fish die.',
				icon: 'SymbioticTank',
			}
		]
	}
}

export const about = {
	name: 'About',
	id: 'about',
	description: 'About the project',
	componentName: 'About',
	iconName: 'About',
	routes: ['/About'],
	displayHeader: false,
	displayFooter: true,
	displayNavTray: true,
	
	content: {
		summaryTitle: 'Concept',
		summaryText: [
			'IchorOS is a machine learning experiment of natural selection in multiple',
			'environments called tanks. Each tank contains a unique challenge the algorithms',
			'called fish must overcome in order to survive.'
		].join(' '),

		rulesTitle: 'Game Rules',
		rulesText: 'Each tank has a unique set of rules, some constants exist across all tanks.',

		hungerTitle: 'Hunger',
		hungerSummary: [
			'Fish must consume food to continue living. Provided infinite food a fish can',
			'live forever. If a fish cannot completely consume a corpse, the remainder is',
			'left over.'
		].join(' '),
		hungerBullets: [
			'All fish are carnivores',
			'Fish must consume food at regular intervals to avoid death',
			'A fish can live indefinitely provided constant food',
			'Maximum hunger is constant among all fish.'
		],

		combatTitle: 'Combat',
		combatSummary: [
			'Fish can attack other fish. Each fish has a pool of health that restores over time.',
			'If health depletes to zero, the fish is killed.'
		].join(' '),
		combatBullets: [
			'Fish can attack other fish',
			'Attacking another fish costs hunger',
			'Maximum health is a function inversely proportional to damage inflicted',
			'Healing rate is a function proportional to maximum health'
		],

		deathTitle: 'Death',
		deathSummary: [
			'If a fish dies, it leaves behind a corpse and respawns at a random location.',
			'The corse the fish leaves can be consumed for food.'
		].join(' '),
		deathBullets: [
			'Dying leaves behind a corpse',
			'The food worth of the corpse is proportional to the killed fish\'s maximum health',
			'Death triggers advancement for the fish',
			'The fish will respawn elsewhere with full stats'
		],
		advancementTitle: 'Advancement',
		advancementSummary: [
			'As time passes without the fish dying, the fish qualifies for advancement',
			'The fish reevaluates it\'s behaviour and variable stats. This occurs',
			'periodically starting from spawn time of the fish.'
		].join(' '),
		advancementBullets: [
			'Fish can change their behavior and stats if they stay alive for long enough',
			'Fish can change their behavior and stats on death',
		]
	}
}

export const notFound = {
	name: 'Page not found',
	id: 'notFound',
	description: '404 - Page not found',
	componentName: 'NotFound',
	iconName: 'About',
	routes: [],
	displayHeader: false,
	displayFooter: false,
	displayNavTray: false,
}
