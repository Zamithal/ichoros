import { createMuiTheme } from '@material-ui/core';
// eslint-disable-next-line no-unused-vars
import audiowide from 'typeface-audiowide';
import { blue, red, grey } from '@material-ui/core/colors';
export const standard = createMuiTheme({

	palette: {
		type: 'light',
		primary: {
			main: blue['600']
		},
		error: {
			main: red['500']
		},
		text: {
			primary: grey['50'],
		},
		background: {
			paper: grey['900'],
		},
		headerLight: grey['800'],
		headerDark: grey['900'],
		headerText: 'white',
	},
	
	typography: {
		useNextVariants: true,
		fontSize: 14,
		fontFamily: [ 'Futura', 'Roboto' ].join(', ')
	},

	spacing: {
		unit: 8,
		small: 8,
		medium: 16,
		large: 24,
	},
	overrides: {
		MuiTypography: {
			h1: {
				fontSize: '2rem',
				fontFamily: 'Audiowide',
			},
			h2: {
				fontSize: '1.7rem',
				fontWeight: 'bold',
			},
			h3: {
				fontSize: '1.5rem',
				fontWeight: 'bold',
			},
			body2: {
				fontSize: '1.05rem',
			}
		}
	}
});